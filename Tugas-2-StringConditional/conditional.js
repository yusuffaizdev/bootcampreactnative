//challenge one
var nama = "John";
var peran = "";

if (!nama) {
  // Output untuk Input nama = '' dan peran = ''
  console.log("Nama harus diisi!");
}
if (!peran) {
  //Output untuk Input nama = 'John' dan peran = ''
  console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
}
if (peran == "Penyihir") {
  var valuePeran = "kamu dapat melihat siapa yang menjadi werewolf!";
}
if (peran == "Guard") {
  var valuePeran =
    "kamu akan membantu melindungi temanmu dari serangan werewolf.";
}
if (peran == "Werewolf") {
  var valuePeran = "Kamu akan memakan mangsa setiap malam!";
}
if (nama && peran) {
  var result = `Halo ${peran} ${nama}, ${valuePeran}`;

  console.log(result);
}

//challenge two
var tanggal = 31; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 05; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1995; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch (bulan) {
  case 1: {
    console.log(`${tanggal} Januari ${tahun}`);
    break;
  }
  case 2: {
    console.log(`${tanggal} Februari ${tahun}`);
    break;
  }
  case 3: {
    console.log(`${tanggal} Maret ${tahun}`);
    break;
  }
  case 4: {
    console.log(`${tanggal} April ${tahun}`);
    break;
  }
  case 5: {
    console.log(`${tanggal} Mei ${tahun}`);
    break;
  }
  case 6: {
    console.log(`${tanggal} Juni ${tahun}`);
    break;
  }
  case 7: {
    console.log(`${tanggal} Juli ${tahun}`);
    break;
  }
  case 8: {
    console.log(`${tanggal} Agustus ${tahun}`);
    break;
  }
  case 9: {
    console.log(`${tanggal} September ${tahun}`);
    break;
  }
  case 10: {
    console.log(`${tanggal} Oktober ${tahun}`);
    break;
  }
  case 11: {
    console.log(`${tanggal} November ${tahun}`);
    break;
  }
  case 12: {
    console.log(`${tanggal} Desember ${tahun}`);
    break;
  }
  default: {
    console.log("Tolong lengkapi tanggal bulan dan tahunnya dengan benar");
  }
}
