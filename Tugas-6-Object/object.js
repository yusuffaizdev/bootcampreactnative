function arrayToObject(datas) {
  var now = new Date();
  var thisYear = now.getFullYear();

  datas.forEach((element) => {
    var fullname = `${element[0]} ${element[1]}`;
    var result = {};
    result.firstName = element[0];
    result.lastName = element[1];
    result.gender = element[2];
    result.age =
      element[3] != undefined ? thisYear - element[3] : "Invalid birth year";
    console.log(`${fullname} : `, result);
  });
}

var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);

function shoppingTime(memberId, money) {
  var allProduct = [
    ["Sepatu brand Stacattu", 1500000],
    ["Baju brand Zoro", 500000],
    ["Baju brand H&N", 250000],
    ["Sweater brand Uniklooh", 175000],
    ["Casing Handphone", 50000],
  ];

  var changeMoney = money;
  var checkout = [];

  if (memberId) {
    if (changeMoney >= 50000) {
      allProduct.forEach((element) => {
        if (changeMoney >= element[1]) {
          checkout.push(element[0]);

          changeMoney = changeMoney - element[1];
        }
      });
      var result = {};

      result.memberId = memberId;
      result.money = money;
      result.listPurchased = checkout;
      result.changeMoney = changeMoney;

      return result;
    } else {
      return `Mohon maaf, uang tidak cukup`;
    }
  } else {
    return `Mohon maaf, toko X hanya berlaku untuk member saja`;
  }
}

console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000));
console.log(shoppingTime("234JdhweRxa53", 15000));
console.log(shoppingTime());

function naikAngkot(listPenumpang) {
  const rute = ["A", "B", "C", "D", "E", "F"];
  const defaultPrice = 2000;
  var result = [];
  listPenumpang.forEach((element) => {
    var log1 = 0;
    var log2 = 0;

    rute.forEach((rut, index) => {
      if (rut == element[1]) {
        log1 = index;
      }
      if (rut == element[2]) {
        log2 = index;
      }
    });
    var object = {};
    object.penumpang = element[0];
    object.naikDari = element[1];
    object.tujuan = element[2];
    object.bayar = defaultPrice * (log2 - log1);
    result.push(object);
  });
  return result;
}

console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
