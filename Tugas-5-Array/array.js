function range(startNum, finishNum) {
  if (startNum && finishNum) {
    var ans = [];
    if (startNum < finishNum) {
      var x = startNum;
      var y = finishNum;
      for (let i = x; i <= y; i++) {
        ans.push(i);
      }
      ans.sort(function (value1, value2) {
        return value1 - value2;
      });
      return ans;
    }
    if (startNum > finishNum) {
      var x = finishNum;
      var y = startNum;
      for (let i = x; i <= y; i++) {
        ans.push(i);
      }
      ans.sort(function (value1, value2) {
        return value2 - value1;
      });
      return ans;
    }
  } else {
    return -1;
  }
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

function rangeWithStep(startNum, finishNum, step) {
  var ans = [];
  if (startNum < finishNum) {
    var x = startNum;
    var y = finishNum;
    for (let i = x; i <= y; i += step) {
      ans.push(i);
    }
    ans.sort(function (value1, value2) {
      return value1 - value2;
    });
    return ans;
  }
  if (startNum > finishNum) {
    var x = finishNum;
    var y = startNum;
    for (let i = x; i <= y; i += step) {
      ans.push(i);
    }
    ans.sort(function (value1, value2) {
      return value2 - value1;
    });
    return ans;
  }
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

function sum(start, end, step = 1) {
  if (start && end) {
    var result = rangeWithStep(start, end, step);
    return result.reduce((a, b) => a + b, 0);
  }
  if (start) {
    return start;
  } else {
    return 0;
  }
}

console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

function dataHandling() {
  var input1 = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
  ];

  input1.forEach((element) => {
    console.log(
      `Nomor ID: ${element[0]}\nNama Lengkap: ${element[1]}\nTTL: ${element[2]} ${element[3]}\nHobi: ${element[4]}\n\n`
    );
  });
}
dataHandling();

function balikKata(s) {
  return s.split("").reverse().join("");
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

function dataHandling2(input) {
  input.splice(1, 1, `${input[1]} Elsharawy`);
  input.splice(2, 1, `Provinsi ${input[2]}`);
  input.splice(4, 1, `Pria`);
  input.splice(5, 1, `SMA Internasional Metro`);
  console.log(input);

  var date = input[3].split("/");

  switch (date[1]) {
    case "01": {
      console.log(`Januari`);
      break;
    }
    case "02": {
      console.log(`Februari`);
      break;
    }
    case "03": {
      console.log(`Maret`);
      break;
    }
    case "04": {
      console.log(`April`);
      break;
    }
    case "05": {
      console.log(`Mei`);
      break;
    }
    case "06": {
      console.log(`Juni`);
      break;
    }
    case "07": {
      console.log(`Juli`);
      break;
    }
    case "08": {
      console.log(`Agustus`);
      break;
    }
    case "09": {
      console.log(`September`);
      break;
    }
    case "10": {
      console.log(`Oktober`);
      break;
    }
    case "11": {
      console.log(`November`);
      break;
    }
    case "12": {
      console.log(`Desember`);
      break;
    }
    default: {
      console.log("Tolong lengkapi tanggal bulan dan tahunnya dengan benar");
    }
  }
  date.sort(function (value1, value2) {
    return value2 - value1;
  });

  console.log(date);

  var date2 = input[3].split("/");
  console.log(date2.join("-"));

  console.log(input[1].slice(0, 15));
}

var input = [
  "0001",
  "Roman Alamsyah",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca",
];
dataHandling2(input);
