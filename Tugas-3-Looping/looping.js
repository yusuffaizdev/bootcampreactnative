//challenge one
var deret = 20;
var jumlah = 2;
console.log("LOOPING PERTAMA");
while (deret > 1) {
  console.log(`${jumlah} - I love coding`);
  jumlah += 2;
  deret -= 2;
}

if (deret < 2) {
  var deret2 = 20;
  var jumlah2 = 2;
  console.log("LOOPING PERTAMA");
  while (deret2 > 1) {
    console.log(`${deret2} - I will become a mobile developer`);
    jumlah2 += 2;
    deret2 -= 2;
  }
}

//challenge two
for (var angka = 1; angka < 21; angka++) {
  if (angka % 2 != 0) {
    var label = "Santai";
  }
  if (angka % 2 == 0) {
    var label = "Berkualitas";
  }
  if (angka % 3 == 0 && angka % 2 != 0) {
    var label = "I Love Coding";
  }
  console.log(`${angka} - ${label}`);
}

//challenge three

var totalNumberofRowsOne = 4;
var output = "";

for (var x = 1; x <= totalNumberofRowsOne; x++) {
  output += "########";
  console.log(output);
  output = "";
}

//challenge four
var totalNumberofRowsTwo = 7;
var outputStrTwo = "";

for (var i = 1; i <= totalNumberofRowsTwo; i++) {
  for (var j = 1; j <= i; j++) {
    outputStrTwo += "#";
  }
  console.log(outputStrTwo);
  outputStrTwo = "";
}

//challenge five
var size = 8;
var board = "";

for (var y = 0; y < size; y++) {
  for (var x = 0; x < size; x++) {
    if ((x + y) % 2 == 0) board += " ";
    else board += "#";
  }
  board += "\n";
}
console.log(board);
