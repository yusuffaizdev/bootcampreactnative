let readBooks = require("./callback.js");

let books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];
let x = 10000;

readBooks(x, books[0], function (check) {
  x = check;
  if (check) {
    readBooks(x, books[1], function (check) {
      x = check;
      if (check) {
        readBooks(x, books[2], function (check) {
          x = check;
        });
      }
    });
  }
});
